﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ROKUConnection;
using ROKUConnection.Controllers;
using ROKUConnection.Models;
using System.Data.Entity;

namespace ROKUConnection.Tests.Controllers
{
    [TestClass]
    public class ROKUControllerTest
    {
        //DbContext db = new DbContext("ROKU");

        DbContext rokuboxes = new ROKU();

        [TestMethod]
        public void GetRegistrationCode()
        {
            // Arrange
            RokuController controller = new RokuController();

            // Act
            RokuResult result = controller.GetRegCode(null);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CheckRegistrationCode()
        {
            // Arrange
            RokuController controller = new RokuController();

            HttpRequestMessage msg = new HttpRequestMessage();
            var tmsg = "{'deviceID':'uniqueDeviceID','deviceTypeID':'ZSuperRokuBox','deviceFirmwareVersion':'Version5'}";
            msg.Content = new StringContent(tmsg);

            RokuResult getrdun = controller.GetRegCode(msg);

            msg = new HttpRequestMessage();
            var rmsg = "{'deviceID':'uniqueDeviceID','regCode':'";
            rmsg += getrdun.rokubox.regCode;
            rmsg += "','allowDonations':1}";            
            msg.Content = new StringContent(rmsg);

            // Act
            RokuResult result = controller.CheckRegCode(msg);

            // Assert
            Assert.IsTrue(result.status.Equals(ROKUStatus.success.ToString()));

            using (ROKU rk = new ROKU())
            {
                rk.RokuBoxes.Attach(result.rokubox);
                rk.RokuBoxes.Remove(result.rokubox);
                rk.SaveChanges();
            }

        }

        [TestMethod]
        public void ROKUBOXWriteTest()
        {
            string rdeviceID = "writeTestDeviceID";
            string rdeviceTypeID = "writeTestDeviceTypeID";
            string rdeviceFirmwareVersion = "writeTestFIRMWARE-55555.4444.333.22.11.0";

            RokuResult result = new RokuResult();

            DateTime regTimeStamp = DateTime.Now;

            string ROKUAuthority = ConfigurationManager.AppSettings["ROKUAuthorityBase"] ?? "GETV";

            var sec = ConfigurationManager.AppSettings["ROKURegCodeExpirationSeconds"];
            Double ROKURegCodeExpirationSeconds = Convert.ToDouble(sec);

            //var poll = ConfigurationManager.AppSettings["ROKURegCodeRetryInterval"];
            //int ROKURegCodeRetryInterval = Convert.ToInt32(poll);
            var newRegCode = new RokuController().CreateNewRegistrationCode();

            var rokubox = new RokuBox()
            {
                accountID = "0",
                allowDonations = true,
                deviceID = rdeviceID,
                deviceToken = "0",
                deviceTypeID = rdeviceTypeID,
                deviceFirmwareVersion = rdeviceFirmwareVersion,
                id = Guid.NewGuid(),
                regCode = newRegCode,
                regCodeExpirationDate = regTimeStamp.AddSeconds(ROKURegCodeExpirationSeconds).ToUniversalTime(),
                createdBy = ROKUAuthority,
                dateCreated = regTimeStamp,
                modifiedBy = ROKUAuthority,
                dateModified = regTimeStamp,
                isActive = false
            };

            rokuboxes.Set<RokuBox>().Add(rokubox);

            rokuboxes.SaveChanges();

            var foundbox = rokuboxes.Set<RokuBox>().FirstOrDefault(b => b.id == rokubox.id);

             Assert.AreEqual(rokubox, foundbox as RokuBox);

            rokuboxes.Set<RokuBox>().Remove(rokubox);
            rokuboxes.SaveChanges();
        }

    }
}
