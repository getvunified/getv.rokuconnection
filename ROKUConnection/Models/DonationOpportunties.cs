﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ROKUConnection.Models
{
    public class DonationOpportunity
    {
        public string id { get; set;}

        public string Name { get; set; }

        public string Description { get; set; }

        public string DonationType { get; set; }

        public decimal MinAmount { get; set; }

        public decimal MaxAmount { get; set; }

        public List<Decimal> Amounts { get; set; }

        public bool allowOtherAmount { get; set; }

        public bool isRecurring { get; set; }

        public bool isActive { get; set; }
        
    }

    public class DonationOpportunties
    {
        public List<DonationOpportunity> Opportunties { get; set; }
    }

}