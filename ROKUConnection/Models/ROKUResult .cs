﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ROKUConnection.Models
{
    public class RokuResult
    {
        public string status { get; set; }

        public RokuBox rokubox { get; set; }

        public List<RokuDonation> donations { get; set; }

        public DonationOpportunties donationopportunies { get; set; }
        
        public int? retryInterval { get; set; }

        public int? retryDuration { get; set; }

        public string errorMessage { get; set; }

        public string errorDetails { get; set; }

        public string errorCode { get; set; }
    }

    public enum ROKUStatus
    {   
        success,
        busy,
        incomplete,
        fail
    }

}