﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace ROKUConnection.Models
{
    public class RokuDonation : BaseEntity
    {
        // Who - Primary 
        public string deviceToken { get; set; }         // REQUIRED

        // Who - Secondary - can also donate using DonorAccountID and deviceID and amount
        public string accountID { get; set; }

        public string deviceID { get; set; }

        public string cardSecureToken { get; set; }          // Open for future security      

        // What 
        public string donationAmount { get; set; }      // REQUIRED!

        public string donationProject { get; set; }         // future use, different donations to diff projects

        public bool isRecurring { get; set; }               // future use, set up recurring via ROKU

        public string relatedURL { get; set; }              // future use, URL request came from or destination for PPV
        // Where
        public string country { get; set; }

        public string IPaddress { get; set; }

        // When - mostly covered in the BaseEntity
        public bool isProcessed { get; set; }

        public DateTime? processedOnDate { get; set; }

        public string processResultCode { get; set; }
    }
}