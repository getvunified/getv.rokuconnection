﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace ROKUConnection.Models
{
    public class RokuBox : BaseEntity
    {
        public string deviceID { get; set; }

        public string regCode { get; set; }

        public DateTime? regCodeExpirationDate { get; set; }

        public string deviceName { get; set; }
        
        public string accountID { get; set; }

        public string deviceToken { get; set; }

        public string deviceTypeID { get; set; }

        public string deviceFirmwareVersion { get; set; }

        public bool allowDonations { get; set; }
    }
}