﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ROKUConnection.Models
{
   public class BaseEntity
   {
        [Key]
        [ScaffoldColumn(false)]
        public Guid? id { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [DisplayName("Date Created")]
        public DateTime? dateCreated { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [DisplayName("Date Modified")]
        public DateTime? dateModified { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [DisplayName("Created By")]
        //[MaxLength(50)]
        public string createdBy { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [DisplayName("Modified By")]
        //[MaxLength(50)]
        public string modifiedBy { get; set; }

        [Required]
        [DisplayName("Is Active?")]
        public bool isActive { get; set; }

        /// <summary>
        /// Checks if the provided object is equal to the current Entity
        /// </summary>
        /// <param name="obj">Object to compare to the current Entity</param>
        /// <returns>True if equal, false if not</returns>
        public override bool Equals(object obj)
        {
            // Try to cast the object to compare to to be an Entity
            var entity = obj as BaseEntity;

            return Equals(entity);
        }

        /// <summary>
        /// Returns an identifier for this instance
        /// </summary>
        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        /// <summary>
        /// Checks if the provided Person is equal to the current Entity
        /// </summary>
        /// <param name="entityToCompareTo">Person to compare to the current Entity</param>
        /// <returns>True if equal, false if not</returns>
        public bool Equals(BaseEntity entityToCompareTo)
        {
            // Check if entity is being compared to a non entity. In that case always return false.
            if (entityToCompareTo == null) return false;

            // If the entity to compare to does not have an Id assigned yet, we can't define if it's the same. Return false.
            if (!entityToCompareTo.id.HasValue) return false;

            // Check if both peentityrson objects contain the same NamIde. In that case they're assumed equal.
            return id.Equals(entityToCompareTo.id);
        }

       public void Initialize()
       {
           id = id ?? Guid.NewGuid();
           createdBy = createdBy ?? ConfigurationManager.AppSettings["ROKUAuthorityBase"] ?? "GETV.Web";
           modifiedBy = modifiedBy ?? ConfigurationManager.AppSettings["ROKUAuthorityBase"] ?? "GETV.Web";
           dateCreated = dateCreated ?? DateTime.UtcNow;
           dateModified = dateModified ?? dateCreated;           
       }
   }
}