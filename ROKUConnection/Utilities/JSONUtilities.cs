﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ROKUConnection.Utilities
{
    public class JSONUtilities
    {
        public static string GetJSONFromRequestBody(HttpRequestMessage request)
        {
            var contentType = request.Content.Headers.ContentType;
            var contentInString = request.Content.ReadAsStringAsync().Result;
            request.Content = new StringContent(contentInString);
            request.Content.Headers.ContentType = contentType;
            return contentInString;
        }


    }
}