﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using ROKUConnection.Models;

namespace ROKUConnection.Controllers
{
    public class HomeController : Controller
    {
        DbContext db = new ROKU();

        public ActionResult Index(RokuBox rkupload = null)
        {
            RokuBox rkinfo = rkupload ?? new RokuBox();
            
            // Initialize the reg page object
            RokuBox roku = new RokuBox();
            roku.accountID = "0";
            roku.deviceName = String.Empty;
            roku.regCode = String.Empty;

            
            roku.deviceID = rkinfo.deviceID ?? String.Empty;
            roku.deviceTypeID = rkinfo.deviceTypeID ?? String.Empty;
            roku.deviceFirmwareVersion = rkinfo.deviceFirmwareVersion ?? String.Empty;

            ViewBag.ErrorMessage = String.Empty;

            return View(roku);
        }

        public ActionResult Register(RokuBox roku)
        {
            var result = new RokuResult();

            // Validate
            if (roku.deviceName.IsNullOrWhiteSpace())
            {
                ViewBag.ErrorMessage = "Device Name is required. Please enter a Device Name.";
                return View("Index", roku);
            }

            roku.deviceName = roku.deviceName.Trim();
            if (roku.regCode.IsNullOrWhiteSpace())
            {
                ViewBag.ErrorMessage = "Registration Code is required. Please enter a Registration Code.";
                return View("Index", roku);
            }

            roku.regCode = roku.regCode.Trim().ToUpper();

            var findRokusByRegCodes = db.Set<RokuBox>().Where(b => b.regCode == roku.regCode);
            if (!findRokusByRegCodes.Any())
            {
                result.errorMessage = String.Format("The Registration Code '{0}' was not found.", roku.regCode);
                result.errorDetails = " Please check the Registration Code on your Roku device screen and click Registration above to try again.";
                result.rokubox = roku;
                return View("Error", result);
            }

            var findRokusByExp = findRokusByRegCodes.Where(r => r.regCodeExpirationDate > DateTime.UtcNow);
            if (!findRokusByExp.Any())
            {
                result.errorMessage = "The Registration Code has expired.";
                result.errorDetails = "Please get another new Registration Code from your Roku device.";
                result.rokubox = roku;
                return View("Error", result);
            }

            // TODO  Handle multiple registration attempts from different boxex with the EXACT same RegCode (very very unlikely) 

            RokuBox foundRoku = findRokusByRegCodes.FirstOrDefault();

            // Add donor account and device name to existing roku device, as set up in RokuController GetRegCode
            foundRoku.accountID = String.Format("donoraccount|{0}|{1}",roku.deviceName, roku.regCode);           // TODO connect to Donor account !!!
            foundRoku.deviceName = roku.deviceName;
            foundRoku.allowDonations = roku.allowDonations;
            foundRoku.isActive = true;
            foundRoku.dateModified = DateTime.UtcNow;
            foundRoku.modifiedBy = ConfigurationManager.AppSettings["ROKUAuthorityBase"] ?? "GETV";
            foundRoku.modifiedBy += ".WebRegistration";

            try
            {
                db.SaveChanges();
            }
            catch (Exception dx)
            {
                result.errorMessage = String.Format("Unable to register the code {0}.", roku.regCode);
                result.errorDetails = " An unexpected error occured in registration. Please try again later.";
                result.rokubox = roku;
                return View("Error", result);
            }

            return View("Registration", roku);
        }
    }
}
