﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;
using System.Web.Razor.Generator;
using System.Xml.Linq;
using Microsoft.Ajax.Utilities;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using ROKUConnection.Models;
using ROKUConnection.Utilities;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace ROKUConnection.Controllers
{
        public class RokuController : ApiController
        {
            DbContext db = new ROKU();
           //  private static readonly ILog log = LogManager.GetLogger(typeof(RokuController));

            [System.Web.Http.HttpPost]            
            public RokuResult GetRegCode(HttpRequestMessage request) 
            {
                // check request body for deviceID and other metadata
                string rdeviceID = null;
                string rdeviceTypeID = null;
                string rdeviceFirmwareVersion = null;

                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                DateTime regTimeStamp = DateTime.Now;

                string ROKUAuthority = ConfigurationManager.AppSettings["ROKUAuthorityBase"] ?? "GETV";

                try
                {
                        var json = JSONUtilities.GetJSONFromRequestBody(request);
                        if (!json.IsNullOrWhiteSpace())
                        {
                            var requestdetails = JsonConvert.DeserializeObject<dynamic>(json);
                            if (requestdetails != null)
                            {
                                try
                                {
                                    // populate metadata
                                    rdeviceID = requestdetails.deviceID;
                                    rdeviceTypeID = requestdetails.deviceTypeID;
                                    rdeviceFirmwareVersion = requestdetails.deviceFirmwareVersion;
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Processing Error: device parameters", ex.InnerException);
                                }
                            }
                            else // json popularted but will not deserialize parameters
                            {
                                throw new Exception("Processing Error: device parameters", new Exception("Unable to deserialize parameters. There may be a spelling error within device parameters. Please check and try again."));
                            }
                        }
                        else // no request header!
                        {
                            throw new Exception("Processing Error: device parameters", new Exception("No device parameters were provided, unable to pre-register the device and issue a RegCode"));
                        }

                        var sec = ConfigurationManager.AppSettings["ROKURegCodeExpirationSeconds"] ?? "1200";
                        Double ROKURegCodeExpirationSeconds = Convert.ToDouble(sec);

                        var poll = ConfigurationManager.AppSettings["ROKURegCodeRetryInterval"] ?? "3";
                        int ROKURegCodeRetryInterval = Convert.ToInt32(poll);
                        var newRegCode = CreateNewRegistrationCode();
                   
                        result.status = ROKUStatus.success.ToString();
                        result.rokubox = new RokuBox()
                        {
                            accountID = "0",
                            allowDonations = false,
                            deviceID = rdeviceID,
                            deviceToken = "0",
                            deviceTypeID = rdeviceTypeID,
                            deviceFirmwareVersion = rdeviceFirmwareVersion,
                            id = Guid.NewGuid(),
                            regCode = newRegCode,
                            regCodeExpirationDate = regTimeStamp.AddSeconds(ROKURegCodeExpirationSeconds).ToUniversalTime(),
                            createdBy = ROKUAuthority,
                            dateCreated = regTimeStamp,
                            modifiedBy = ROKUAuthority,
                            dateModified = regTimeStamp,
                            isActive = false
                        };

                        db.Set<RokuBox>().Add(result.rokubox);

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception dx)
                        {
                            throw new Exception("Error saving Roku device.");
                            //throw new Exception("Error saving Roku device.", dx);
                        }
                                        
                        result.retryDuration = Convert.ToInt16(ROKURegCodeExpirationSeconds);
                        result.retryInterval = ROKURegCodeRetryInterval;

                        result.status = ROKUStatus.success.ToString();
                }
                catch (Exception ext)
                {
                    // TODO log error

                    result.errorMessage = ext.Message;
                    result.errorDetails = String.Format("{0} >> {1}", ext.Source, ext.InnerException);
                }

                return result;
            }

            [System.Web.Http.HttpGet]
            public RokuResult GetDonationOpportunities()
            {
                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                DonationOpportunties opps = new DonationOpportunties();
                opps.Opportunties = new List<DonationOpportunity>();

                try
                {
                    // Pull from Donor API 
                }
                catch (Exception)
                {
                    // report issue
                    result.errorMessage = "Lookup Error";
                    result.errorDetails = "GetDonationOpportunities >> An unexpected error occured processing the request. Only basic opportunities might be available.";                
                }         

                if (!opps.Opportunties.Any())
                {
                    opps.Opportunties = GetSeedOpportunities();
                }

                result.donationopportunies = opps;
                result.status = ROKUStatus.success.ToString();
               
                return result;
            }

            private List<DonationOpportunity> GetSeedOpportunities()
            {
                List<DonationOpportunity> seedopps = new List<DonationOpportunity>();

                // If service is down, revert to basic donation Opportunities for ROKU

                DonationOpportunity SOH = new DonationOpportunity();
                SOH.id = "SOH";
                SOH.Name = "Sanctuary of Hope";
                SOH.Description = "Donate to Santuary of Hope";
                SOH.DonationType = "ONETIME";
                SOH.MinAmount = 1.00M;
                SOH.MaxAmount = 10000.00M;
                SOH.Amounts = new List<decimal>();
                SOH.Amounts.Add(20.00M);
                SOH.Amounts.Add(50.00M);
                SOH.allowOtherAmount = true;
                SOH.isActive = true;

                seedopps.Add(SOH);

                DonationOpportunity SALT = new DonationOpportunity();
                SALT.id = "SALT";
                SALT.Name = "Salt Partner";
                SALT.Description = "Make Salt Partner Donation";
                SALT.DonationType = "ONETIME";
                SALT.MinAmount = 1.00M;
                SALT.MaxAmount = 10000.00M;
                SALT.Amounts = new List<Decimal>();
                SALT.Amounts.Add(25.00M);
                SALT.Amounts.Add(50.00M);
                SALT.Amounts.Add(75.00M);
                SALT.Amounts.Add(100.00M);
                SALT.allowOtherAmount = true;
                SALT.isActive = true;

                seedopps.Add(SALT);

                DonationOpportunity GETV = new DonationOpportunity();
                GETV.id = "GETV";
                GETV.Name = "GETV";
                GETV.Description = "Donate to GETV";
                GETV.DonationType = "ONETIME";
                GETV.MinAmount = 1.00M;
                GETV.MaxAmount = 10000.00M;
                GETV.Amounts = new List<decimal>();
                GETV.Amounts.Add(20.00M);
                GETV.Amounts.Add(50.00M);
                GETV.allowOtherAmount = true;
                GETV.isRecurring = false;
                GETV.isActive = true;

                seedopps.Add(GETV);


                // simulate ordering and prioritzation by the Commerce API call.

                seedopps = seedopps.OrderBy(i=> i.id).ToList();

                var idx = seedopps.FindIndex(g => g.id == "GETV");
                if (idx != 0)
                {
                    var item = seedopps[idx];
                    seedopps[idx] = seedopps[0];
                    seedopps[0] = item;
                }
                

                return seedopps;
            }

            public string CreateNewRegistrationCode()
            {
                string regCode = string.Empty;
                int codeLen;

                var validCharStrings = ConfigurationManager.AppSettings["ROKURegCodeLegalCharacters"] ?? "ABCDEFGHKMNPQRSTUVWXYZ23456789";  
                var codeLengthSTR = ConfigurationManager.AppSettings["ROKURegCodeLength"] ?? "6";  
                var result = int.TryParse(codeLengthSTR, out codeLen);
                
                var rnd = new Random(DateTime.Now.Millisecond);
                for (int i = 1; i < codeLen; i++)
                {            
                    int idx = rnd.Next(0, validCharStrings.Length-1);
                    regCode += validCharStrings.Substring(idx, 1);
                }

                return regCode;
           }

            [System.Web.Http.HttpPost]
            public RokuResult CheckRegCode(HttpRequestMessage request)
            {
                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                RokuBox rokubox = new RokuBox();

                try  // to get rokuBox and reg info from http body payload
                {
                    var json = JSONUtilities.GetJSONFromRequestBody(request);
                    if (json.IsNullOrWhiteSpace())
                    {
                        throw new Exception("No data provided.", new Exception("There was no body in the request."));                                          
                    }

                    var requestdetails = JsonConvert.DeserializeObject<dynamic>(json);
                    if (requestdetails == null)
                    {
                        throw new Exception("No parameters found", new Exception("There were no parameters found in the body of the request."));
                    }

                    try  // to parse the provided parameters
                    {
                        rokubox.regCode = requestdetails.regCode;
                        rokubox.deviceID = requestdetails.deviceID;
                    }
                    catch (Exception rx)
                    {
                        throw new Exception("Error reading parameters", rx.InnerException);
                    }
                           
                    var matchingboxes = db.Set<RokuBox>()
                        .Where(b=>b.deviceID == rokubox.deviceID)
                        .ToList();

                    if (matchingboxes.Any())                    
                    {
                        foreach (var bx in matchingboxes)
                        {
                            // retain history but clear previous active registration attempts
                            bx.isActive = false;
                        }

                        var foundBox = matchingboxes.FirstOrDefault(b=>b.regCode == rokubox.regCode);

                        if (foundBox == null)
                        {
                            throw new Exception("Not found.", new Exception(String.Format("Unable to find a Roku box matching deviceID {0} and regCode {1} with expiration date {2}.", rokubox.deviceID, rokubox.regCode, rokubox.regCodeExpirationDate)));
                        }

                        if (foundBox.regCodeExpirationDate <= DateTime.UtcNow)
                        {
                            throw new Exception("Expired Registration Code.", new Exception(String.Format("Registration code is expired.")));
                        }


                        if (foundBox.deviceName.IsNullOrWhiteSpace() && foundBox.accountID=="0")
                        {
                            throw new Exception("Device Not Registered.", new Exception(String.Format("Device Has not been given an Name and registered at with GETV.")));
                        }

                        foundBox.deviceToken = CryptoUtilities.GetHashFromString(String.Format("{0}{1}",foundBox.deviceID, foundBox.id));
                        foundBox.modifiedBy = ConfigurationManager.AppSettings["ROKUAuthorityBase"] ?? "GETV";
                        foundBox.dateModified = DateTime.UtcNow;
                        foundBox.regCode = String.Empty;
                        foundBox.isActive = true;

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbEntityValidationException dbx)
                        {
                            throw new Exception("Error saving donation.", dbx);
                            //throw new Exception("Error saving donation.");
                        }
                        catch (Exception dx)
                        {
                            throw new Exception("Error saving donation.", dx);
                            //throw new Exception("Error saving donation.");
                        }         
                        

                        result.rokubox = foundBox;
                        result.status = ROKUStatus.success.ToString();
                    }
                    else
                    {
                       throw new Exception("Not found.", new Exception(String.Format("Unable to find a Roku box matching deviceID {0} and regCode {1} with expiration date {2}.", rokubox.deviceID, rokubox.regCode, rokubox.regCodeExpirationDate)));
                    }
                }
                catch (Exception gx)
                {
                    // TODO   log error json deserial
                    result.errorMessage = gx.Message;
                    result.errorDetails = String.Format("{0} >> {1}", gx.Source, gx.InnerException);
                }

                return result;
            }

            [System.Web.Http.HttpGet]
            public RokuResult IsValidDeviceToken(HttpRequestMessage request)
            {
                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                var deviceToken = String.Empty;

                try  
                {
                    var json = JSONUtilities.GetJSONFromRequestBody(request);
                    if (json.IsNullOrWhiteSpace())
                    {
                        throw new Exception("No data provided.", new Exception("There was no body in the request."));
                    }

                    var requestdetails = JsonConvert.DeserializeObject<dynamic>(json);
                    if (requestdetails == null)
                    {
                        throw new Exception("No parameters found", new Exception("There were no parameters found in the body of the request."));
                    }

                    try  // to parse the provided parameters
                    {
                        deviceToken = requestdetails.deviceToken;
                    }
                    catch (Exception rx)
                    {
                        throw new Exception("Error reading parameters", rx.InnerException);
                    }


                    var foundToken = db.Set<RokuBox>().Where(r=>r.deviceToken == deviceToken);
                    if (foundToken.Any())
                    {
                        result.status = ROKUStatus.success.ToString();
                    }
                }
                catch (Exception ex)
                {
                    // TODO   log error json deserial                    

                    result.errorMessage = "Lookup Error";
                    result.errorDetails = "IsValidDeviceToken >> An unexpected error occured processing the request.";
                }

                return result;
            }

            [System.Web.Http.HttpPost]
            public RokuResult PostNewDonation(HttpRequestMessage request)
            {
                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                RokuBox rokubox = new RokuBox();
                RokuDonation donation = new RokuDonation();

                try  // to get rokuBox and reg info from http body payload
                {
                    var json = JSONUtilities.GetJSONFromRequestBody(request);
                    if (json.IsNullOrWhiteSpace())
                    {
                        throw new Exception("No data provided.", new Exception("There was no body in the request."));
                    }

                    var requestdetails = JsonConvert.DeserializeObject<dynamic>(json);
                    if (requestdetails == null)
                    {
                        throw new Exception("No parameters found", new Exception("There were no parameters found in the body of the request."));
                    }

                    try  // to parse the provided parameters
                    {
                        //Required
                        donation.deviceToken = requestdetails.deviceToken;
                        donation.deviceID = requestdetails.deviceID;
                        donation.donationAmount = requestdetails.donationAmount;
                        donation.donationProject = requestdetails.donationProject;

                        if (donation.donationAmount.IsNullOrWhiteSpace())
                        {
                            throw new Exception("No donation amount.", new Exception("A donation amount was not found in the body of the request."));
                        }

                        donation.accountID = requestdetails.accountID;                          
                        donation.cardSecureToken = requestdetails.cardSecureToken;   
                
                        if ((donation.deviceToken.IsNullOrWhiteSpace() && donation.deviceID.IsNullOrWhiteSpace()) && (donation.accountID.IsNullOrWhiteSpace() || donation.cardSecureToken.IsNullOrWhiteSpace()))
                        {
                            throw new Exception("Cannot identity an account for donation.", new Exception("No device ID, account ID, or Token(s) were provided to assign the donation to."));                                                
                        }

                        // Optional - future use
                        donation.IPaddress = requestdetails.IPaddress;
                        donation.country = requestdetails.country;
                        donation.isRecurring = requestdetails.isRecurring ?? false;
                        donation.relatedURL = requestdetails.relatedURL;
                        donation.processResultCode = "SUBMITTING";
                        donation.isActive = true;

                        donation.Initialize(); // Prep for Persistence!
                    }
                    catch (Exception rx)
                    {
                        throw new Exception("Error reading parameters", rx.InnerException);
                    }

                    var matchingRoku = db.Set<RokuBox>()
                        .FirstOrDefault(b => (b.deviceID == donation.deviceID || b.deviceToken == donation.deviceToken) && b.isActive);

                    if (matchingRoku != null)
                    {
                        db.Set<RokuDonation>().Add(donation);

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbEntityValidationException dx)
                        {
                            throw new Exception("Error saving donation.", dx);
                            //throw new Exception("Error saving donation.");
                        }
                        catch (Exception dx)
                        {
                            throw new Exception("Error saving donation.", dx);
                            //throw new Exception("Error saving donation.");
                        }

                        result.donations = new List<RokuDonation>();
                        result.donations.Add(donation);

                        result.status = ROKUStatus.success.ToString();

                        if (!matchingRoku.allowDonations)
                        {
                            result.errorMessage = "Donations have been disabled on this device.";
                            result.errorDetails = "The donation was recorded but will not be processed. Please go to GETV.org/roku to enable donations for your Roku device and/or complete this donation.";
                        }
                    }
                    else
                    {
                        throw new Exception("Device not found.", new Exception(String.Format("Unable to find a Roku device to assign this donation.")));
                    }
                }
                catch (Exception gx)
                {
                    // TODO   log error 
                    result.errorMessage = gx.Message;
                    result.errorDetails = String.Format("{0} >> {1}", gx.Source, gx.InnerException);
                }

                // LAUNCH DONOR Processing here in a new thread

                return result;

                // Now poll for success from the device
                //      if busy, then still sending permission for video or processing card
                //      if success, ready to view
            }

            [System.Web.Http.HttpPost]
            public RokuResult GetDonationStatus(HttpRequestMessage request)
            {
                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                RokuDonation donation = new RokuDonation();

                try // to get rokuBox and reg info from http body payload
                {
                    var json = JSONUtilities.GetJSONFromRequestBody(request);
                    if (json.IsNullOrWhiteSpace())
                    {
                        throw new Exception("No data provided.", new Exception("There was no body in the request."));
                    }

                    var requestdetails = JsonConvert.DeserializeObject<dynamic>(json);
                    if (requestdetails == null)
                    {
                        throw new Exception("No parameters found",
                            new Exception("There were no parameters found in the body of the request."));
                    }

                    try // to parse the provided parameters
                    {
                        donation.id = requestdetails.id;
                        donation.deviceToken = requestdetails.deviceToken;
                    }
                    catch (Exception rx)
                    {
                        throw new Exception("Error reading parameters", rx.InnerException);
                    }

                    var matchingDonations = new List<RokuDonation>();

                    if (!donation.id.ToString().IsNullOrWhiteSpace())
                    {
                        matchingDonations = 
                            db.Set<RokuDonation>()
                                    .Where(d => d.id == donation.id)
                                    .ToList();
                    }
                    else
                    {
                            matchingDonations =
                                db.Set<RokuDonation>()
                                    .Where(d => d.deviceToken == donation.deviceToken)
                                    .OrderBy(o=> o.dateCreated)
                                    .ToList();
                    }

                    if (matchingDonations.Any())
                    {
                        result.donations = matchingDonations;
                        result.status = ROKUStatus.success.ToString();
                    }
                    else
                    {
                        result.errorCode = "No Donations Found.";
                        result.errorDetails = String.Empty;
                    }
                }
                catch (Exception gx)
                {
                    // TODO   log error 
                    result.errorMessage = gx.Message;
                    result.errorDetails = String.Format("{0} >> {1}", gx.Source, gx.InnerException);
                }

                return result;
            }

            [System.Web.Http.HttpPost]
            public RokuResult PostDevicePurchase(HttpRequestMessage request)
            {
                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                RokuBox rokubox = new RokuBox();
                RokuDonation donation = new RokuDonation();

                try  // to get rokuBox and reg info from http body payload
                {
                    var json = JSONUtilities.GetJSONFromRequestBody(request);
                    if (json.IsNullOrWhiteSpace())
                    {
                        throw new Exception("No data provided.", new Exception("There was no body in the request."));
                    }

                    var requestdetails = JsonConvert.DeserializeObject<dynamic>(json);
                    if (requestdetails == null)
                    {
                        throw new Exception("No parameters found", new Exception("There were no parameters found in the body of the request."));
                    }

                    try  // to parse the provided parameters
                    {
                        //Required
                        donation.deviceToken = requestdetails.deviceToken;
                        donation.deviceID = requestdetails.deviceID;                    // Helpful but not completely required if deviceToken provided
                        donation.donationProject = requestdetails.donationProject;      // SPECIAL CASE!!! This is the product code for the quickpurchase

                        //if (donation.donationAmount.IsNullOrWhiteSpace())
                        //{
                        //    throw new Exception("No purchase amount.", new Exception("A purchasse amount was not found in the body of the request."));
                        //}

                        // Optional stuff
                        donation.accountID = requestdetails.accountID;
                        donation.donationAmount = requestdetails.donationAmount;    
                        donation.cardSecureToken = requestdetails.cardSecureToken;

                        if ((donation.deviceToken.IsNullOrWhiteSpace() && donation.deviceID.IsNullOrWhiteSpace()) && (donation.accountID.IsNullOrWhiteSpace() || donation.cardSecureToken.IsNullOrWhiteSpace()))
                        {
                            throw new Exception("Cannot identity an account for the purchase.", new Exception("No device ID, account ID, or Token(s) were provided to assign the purchse to."));
                        }

                        // Optional - future use
                        donation.IPaddress = requestdetails.IPaddress;
                        donation.country = requestdetails.country;
                        donation.isRecurring = requestdetails.isRecurring ?? false;
                        donation.relatedURL = requestdetails.relatedURL;            // This is ithe URL for the PPV or product page
                        donation.processResultCode = "SUBMITTING";
                        donation.isActive = true;

                        donation.Initialize(); // Prep for Persistence!
                    }
                    catch (Exception rx)
                    {
                        throw new Exception("Error reading parameters", rx.InnerException);
                    }

                    var matchingRoku = db.Set<RokuBox>()
                        .FirstOrDefault(b => (b.deviceID == donation.deviceID || b.deviceToken == donation.deviceToken) && b.isActive);

                    if (matchingRoku != null)
                    {
                        db.Set<RokuDonation>().Add(donation);

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbEntityValidationException dx)
                        {
                            throw new Exception("Error saving purchase.", dx);
                            //throw new Exception("Error saving donation.");
                        }
                        catch (Exception dx)
                        {
                            throw new Exception("Error saving purchase.", dx);
                            //throw new Exception("Error saving donation.");
                        }

                        result.donations = new List<RokuDonation>();
                        result.donations.Add(donation);

                        result.status = ROKUStatus.success.ToString();

                        if (!matchingRoku.allowDonations)
                        {
                            result.errorMessage = "Purchases have been disabled on this device.";
                            result.errorDetails = "The purchsse was recorded but will not be processed. Please go to GETV.org/roku to enable donations/purchases for your Roku device and/or complete this donation.";
                        }
                    }
                    else
                    {
                        throw new Exception("Device not found.", new Exception(String.Format("Unable to find a Roku device to assign this purchase.")));
                    }
                }
                catch (Exception gx)
                {
                    // TODO   log error 
                    result.errorMessage = gx.Message;
                    result.errorDetails = String.Format("{0} >> {1}", gx.Source, gx.InnerException);
                }


                // LAUNCH DONOR Processing here in a new thread


                return result;

                // Now poll for success from the device
                //      if busy, then still sending permission for video or processing card
                //      if success, ready to view
            }

            [System.Web.Http.HttpPost]
            public RokuResult GetPurchaseStatus(HttpRequestMessage request)
            {
                RokuResult result = new RokuResult();
                result.status = ROKUStatus.fail.ToString();

                RokuDonation donation = new RokuDonation();

                try // to get rokuBox and reg info from http body payload
                {
                    var json = JSONUtilities.GetJSONFromRequestBody(request);
                    if (json.IsNullOrWhiteSpace())
                    {
                        throw new Exception("No data provided.", new Exception("There was no body in the request."));
                    }

                    var requestdetails = JsonConvert.DeserializeObject<dynamic>(json);
                    if (requestdetails == null)
                    {
                        throw new Exception("No parameters found",
                            new Exception("There were no parameters found in the body of the request."));
                    }

                    try // to parse the provided parameters
                    {
                        donation.id = requestdetails.id;
                        donation.deviceToken = requestdetails.deviceToken;
                    }
                    catch (Exception rx)
                    {
                        throw new Exception("Error reading parameters", rx.InnerException);
                    }

                    var matchingDonations = new List<RokuDonation>();

                    if (!donation.id.ToString().IsNullOrWhiteSpace())
                    {
                        matchingDonations =
                            db.Set<RokuDonation>()
                                    .Where(d => d.id == donation.id)
                                    .ToList();
                    }
                    else
                    {
                        matchingDonations =
                            db.Set<RokuDonation>()
                                .Where(d => d.deviceToken == donation.deviceToken)
                                .OrderBy(o => o.dateCreated)
                                .ToList();
                    }

                    if (matchingDonations.Any())
                    {
                        result.donations = matchingDonations;
                        result.status = ROKUStatus.success.ToString();
                    }
                    else
                    {
                        result.errorCode = "No Purchases Found.";
                        result.errorDetails = String.Empty;
                    }
                }
                catch (Exception gx)
                {
                    // TODO   log error 
                    result.errorMessage = gx.Message;
                    result.errorDetails = String.Format("{0} >> {1}", gx.Source, gx.InnerException);
                }

                return result;
            }

            // -----------------------------------------------------------------------------------------------------

            // Future Admin features

            // CLEAR ALL REG CODES

            // REMOVE OLD REGISTRATION ATTEMPTS that are not active

            // AUTO ARCHIVE on UPDATE usign trigger to keep history.
        }

}
