using ROKUConnection.Models;

namespace ROKUConnection
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ROKU : DbContext
    {
        // Your context has been configured to use a 'ROKU' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ROKUConnection.ROKU' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ROKU' 
        // connection string in the application configuration file.
        public ROKU()
            : base("name=ROKU")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public DbSet<RokuBox> RokuBoxes { get; set; }   // virtual?
        public DbSet<RokuDonation> RokuDonations { get; set; } 
    }
}